{
    "@context": "https://doi.org/10.5063/schema/codemeta-2.0",
    "@type": "SoftwareSourceCode",
    "license": "https://spdx.org/licenses/Apache-2.0",
    "codeRepository": "https://git.astron.nl/astron-sdc/escape-wp5/esap-deployment",
    "dateCreated": "2019-06-01",
    "datePublished": "2023-01-31",
    "dateModified": "2023-04-25",
    "downloadUrl": "https://git.astron.nl/astron-sdc/escape-wp5/esap-deployment/-/archive/main/esap-deployment-main.zip",
    "issueTracker": "https://git.astron.nl/astron-sdc/escape-wp5/esap-general/-/issues/",
    "name": "ESFRI Science Analysis Platform",
    "version": "1.0.1",
    "softwareVersion": "1.0.1",
    "identifier": {
        "@type": "URL",
        "url":  "https://doi.org/10.5281/zenodo.7585742"
    },
    "description": "ESAP is a science platform toolkit: an integrated set of software components which ESFRIs, ESCAPE project partners, and other groups can use to rapidly assemble and deploy platforms that are customized to the needs of their particular user communities and which integrate their existing service portfolios. These various deployed instances of ESAP then provide the key interfaces between the services delivered by the ESCAPE project and the wider scientific community.\n",
    "applicationCategory": "Astronomy, Particle Physics",
    "funding": "Grant Agreement number 824064",
    "developmentStatus": "active",
    "isPartOf": "https://projectescape.eu/",
    "referencePublication": "https://doi.org/10.5281/zenodo.6044637",
    "funder": {
        "@type": "Organization",
        "name": "European Union’s Horizon 2020 research and innovation programme "
    },
    "readme": "https://git.astron.nl/astron-sdc/escape-wp5/esap-api-gateway/-/wikis/home",
    "releaseNotes": "Initial release of ESAP with minor changes to codemeta.json.",
    "keywords": [
        "Projects: ESCAPE",
        "CTA",
        "EGO-Virgo",
        "ELT",
        "EST",
        "FAIR",
        "HL-LHC",
        "KM3NeT",
        "LSST",
        "LOFAR",
        "SKA; Content: Astronomy",
        "Astroparticle physics",
        "Particle physics"
    ],
    "programmingLanguage": [
        "Python 3",
        "ReactJS",
        "Django"
    ],
    "softwareRequirements": [
        "Python 3, npm"
    ],
    "relatedLink": [
        "https://git.astron.nl/astron-sdc/escape-wp5/esap-api-gateway",
        "https://git.astron.nl/astron-sdc/escape-wp5/esap-gui",
        "https://git.astron.nl/astron-sdc/escape-wp5/esap-worker"
    ],
    "maintainer": [],
    "author": [
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-2386-623X",
            "givenName": "Sara",
            "familyName": "Bertocco",
            "email": "sara.bertocco@inaf.it",
            "affiliation": {
                "@type": "Organization",
                "name": "INAF - Italian National Institute for Astrophysics"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-5893-1797",
            "givenName": "Catherine",
            "familyName": "Boisson",
            "email": "catherine.boisson@obspm.fr",
            "affiliation": {
                "@type": "Organization",
                "name": "Laboratoire Univers et Théories, Observatoire de Paris, Université PSL, Université Paris Cité, CNRS, F-92190 Meudon, France"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-4887-2150",
            "givenName": "Dominique",
            "familyName": "Boutigny",
            "email": "boutigny@in2p3.fr",
            "affiliation": {
                "@type": "Organization",
                "name": "Laboratoire d'Annecy de Physique des Particules, Univ. Savoie Mont Blanc, CNRS/IN2P3, Annecy, France"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-3843-4130",
            "givenName": "James",
            "familyName": "Collinson",
            "email": "James.Collinson@skao.int",
            "affiliation": {
                "@type": "Organization",
                "name": "SKA Observatory, Jodrell Bank, Lower Withington, Macclesfield SK11 9FT, UK"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-5099-7169",
            "givenName": "Nuria",
            "familyName": "Alvarez Crespo",
            "email": "nuriaa05@ucm.es",
            "affiliation": {
                "@type": "Organization",
                "name": "EMFTEL department and IPARCOS, Universidad Complutense de Madrid, E-28040 Madrid, Spain"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-0475-008X",
            "givenName": "Hugh",
            "familyName": "Dickinson",
            "email": "hugh.dickinson@open.ac.uk",
            "affiliation": {
                "@type": "Organization",
                "name": "The Open University, Walton Hall, Kents Hill, Milton Keynes MK7 6AA, UK"
            }
        },
        {
            "@type": "Person",
            "givenName": "Evgeny",
            "familyName": "Lavrik",
            "email": "E.Lavrik@gsi.de"
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-1394-9242",
            "givenName": "Fanna",
            "familyName": "Lautenbach",
            "email": "lautenbach@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-7503-9821",
            "givenName": "Matthias",
            "familyName": "Füßling",
            "email": "matthias.fuessling@cta-observatory.org",
            "affiliation": {
                "@type": "Organization",
                "name": "Cherenkov Telescope Array Observatory, Saupfercheckweg 1, 69117 Heidelberg, Germany"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-9820-9114",
            "givenName": "Gangadharan",
            "familyName": "Vigeesh",
            "email": "vigeesh@leibniz-kis.de",
            "affiliation": {
                "@type": "Organization",
                "name": "Leibniz-Institut für Sonnenphysik (KIS), Schöneckstraße 6, 79104 Freiburg, Germany"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-7282-2394",
            "givenName": "Jose Luis",
            "familyName": "Contreras",
            "email": "jlcontreras@fis.ucm.es",
            "affiliation": {
                "@type": "Organization",
                "name": "EMFTEL department and IPARCOS, Universidad Complutense de Madrid, E-28040 Madrid, Spain"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-5125-9539",
            "givenName": "Yan",
            "familyName": "Grange",
            "email": "grange@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-7653-6456",
            "givenName": "Gareth",
            "familyName": "Hughes",
            "email": "gareth.hughes@cta-observatory.org",
            "affiliation": {
                "@type": "Organization",
                "name": "Cherenkov Telescope Array Observatory, Saupfercheckweg 1, 69117 Heidelberg, Germany"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-4492-6732",
            "givenName": "Klaas",
            "familyName": "Kliffen",
            "email": "kliffen@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-3861-9234",
            "givenName": "Mattia",
            "familyName": "Mancini",
            "email": "mancini@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-0186-3639",
            "givenName": "Riccardo",
            "familyName": "di Maria",
            "email": "riccardo.di.maria@cern.ch",
            "affiliation": {
                "@type": "Organization",
                "name": "ETH Zurich - CSCS (previously CERN)"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-9108-7081",
            "givenName": "Zheng",
            "familyName": "Meyer",
            "email": "meyer@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-4487-6752",
            "givenName": "Stefano Alberto",
            "familyName": "Russo",
            "email": "stefano.russo@inaf.it",
            "affiliation": {
                "@type": "Organization",
                "name": "INAF - Italian National Institute for Astrophysics"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-6353-0808",
            "givenName": "Volodymyr",
            "familyName": "Savchenko",
            "email": "volodymyr.savchenko@epfl.ch",
            "affiliation": {
                "@type": "Organization",
                "name": "Swiss Federal Institute of Technology Lausanne (EPFL), Rte Cantonale, 1015 Lausanne, Switzerland"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-5443-4128",
            "givenName": "Mathieu",
            "familyName": "Servillat",
            "email": "mathieu.servillat@obspm.fr",
            "affiliation": {
                "@type": "Organization",
                "name": "Laboratoire Univers et Théories, Observatoire de Paris, Université PSL, Université Paris Cité, CNRS, F-92190 Meudon, France"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-9445-1846",
            "givenName": "John",
            "familyName": "Swinbank",
            "email": "swinbank@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-2884-9834",
            "givenName": "Marjolein",
            "familyName": "Verkouter",
            "email": "verkouter@jive.eu",
            "affiliation": {
                "@type": "Organization",
                "name": "Joint Institute for VLBI in Europe: Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "givenName": "Nico",
            "familyName": "Vermaas",
            "email": "vermaas@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "url": "https://www.research.ed.ac.uk/en/persons/stelios-voutsinas",
            "givenName": "Stelios",
            "familyName": "Voutsinas",
            "email": "Stelios.Voutsinas@ed.ac.uk",
            "affiliation": {
                "@type": "Organization",
                "name": "University of Edinburgh, Old College, South Bridge, Edinburgh EH8 9YL, UK"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-5686-2078",
            "givenName": "Thomas",
            "familyName": "Vuillaume",
            "email": "thomas.vuillaume@lapp.in2p3.fr",
            "affiliation": {
                "@type": "Organization",
                "name": "Univ. Savoie Mont Blanc, CNRS, Laboratoire d'Annecy de Physique des Particules - IN2P3, 74000 Annecy, France"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-0156-6180",
            "givenName": "Lourdes",
            "familyName": "Verdes-Montenegro",
            "email": "lourdes@iaa.es",
            "affiliation": {
                "@type": "Organization",
                "name": "Instituto de Astrofísica de Andalucía - CSIC, Glorieta de la Astronomía s/n, 18008, Granada, Spain"
            }
        },
        {
            "@type": "Person",
            "givenName": "Rafael",
            "familyName": "Garrido",
            "email": "garrido@iaa.es",
            "affiliation": {
                "@type": "Organization",
                "name": "Instituto de Astrofísica de Andalucía - CSIC, Glorieta de la Astronomía s/n, 18008, Granada, Spain"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-6696-4772",
            "givenName": "Julián",
            "familyName": "Garrido",
            "email": "jgarrido@iaa.es",
            "affiliation": {
                "@type": "Organization",
                "name": "Instituto de Astrofísica de Andalucía - CSIC, Glorieta de la Astronomía s/n, 18008, Granada, Spain"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-3101-917X",
            "givenName": "José Ramón",
            "familyName": "Rodón",
            "email": "rodon@iaa.es",
            "affiliation": {
                "@type": "Organization",
                "name": "Instituto de Astrofísica de Andalucía - CSIC, Glorieta de la Astronomía s/n, 18008, Granada, Spain"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-0139-6951",
            "givenName": "Javier",
            "familyName": "Pascual",
            "email": "javier@iaa.es",
            "affiliation": {
                "@type": "Organization",
                "name": "Instituto de Astrofísica de Andalucía - CSIC, Glorieta de la Astronomía s/n, 18008, Granada, Spain"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-6275-8242",
            "givenName": "Manuel",
            "familyName": "Parra-Royón",
            "email": "mparra@iaa.es",
            "affiliation": {
                "@type": "Organization",
                "name": "Instituto de Astrofísica de Andalucía - CSIC, Glorieta de la Astronomía s/n, 18008, Granada, Spain"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-7510-7633",
            "givenName": "Susana",
            "familyName": "Sánchez-Exposito",
            "email": "sse@iaa.es",
            "affiliation": {
                "@type": "Organization",
                "name": "Instituto de Astrofísica de Andalucía - CSIC, Glorieta de la Astronomía s/n, 18008, Granada, Spain"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-3143-4798",
            "givenName": "Sebastián",
            "familyName": "Luna-Valero",
            "email": "sebastian.luna.valero@gmail.com",
            "affiliation": {
                "@type": "Organization",
                "name": "EGI, 1098 XG Amsterdam, Netherlands"
            }
        }
    ],
    "contributor": [
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-3843-4130",
            "givenName": "James",
            "familyName": "Collinson",
            "email": "James.Collinson@skao.int",
            "affiliation": {
                "@type": "Organization",
                "name": "SKA Observatory, Jodrell Bank, Lower Withington, Macclesfield SK11 9FT, UK"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-5099-7169",
            "givenName": "Nuria",
            "familyName": "Alvarez Crespo",
            "email": "nuriaa05@ucm.es",
            "affiliation": {
                "@type": "Organization",
                "name": "EMFTEL department and IPARCOS, Universidad Complutense de Madrid, E-28040 Madrid, Spain"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-0475-008X",
            "givenName": "Hugh",
            "familyName": "Dickinson",
            "email": "hugh.dickinson@open.ac.uk",
            "affiliation": {
                "@type": "Organization",
                "name": "The Open University, Walton Hall, Kents Hill, Milton Keynes MK7 6AA, UK"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-1394-9242",
            "givenName": "Fanna",
            "familyName": "Lautenbach",
            "email": "lautenbach@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-1952-6251",
            "givenName": "Dave",
            "familyName": "Morris",
            "email": "dmr@roe.ac.uk",
            "affiliation": {
                "@type": "Organization",
                "name": "Wide Field Astronomy Unit, Institute for Astronomy, University of Edinburgh"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-5125-9539",
            "givenName": "Yan",
            "familyName": "Grange",
            "email": "grange@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-7653-6456",
            "givenName": "Gareth",
            "familyName": "Hughes",
            "email": "gareth.hughes@cta-observatory.org",
            "affiliation": {
                "@type": "Organization",
                "name": "Cherenkov Telescope Array Observatory, Saupfercheckweg 1, 69117 Heidelberg, Germany"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-4492-6732",
            "givenName": "Klaas",
            "familyName": "Kliffen",
            "email": "kliffen@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-3861-9234",
            "givenName": "Mattia",
            "familyName": "Mancini",
            "email": "mancini@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-9108-7081",
            "givenName": "Zheng",
            "familyName": "Meyer",
            "email": "meyer@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0003-4487-6752",
            "givenName": "Stefano Alberto",
            "familyName": "Russo",
            "email": "stefano.russo@inaf.it",
            "affiliation": {
                "@type": "Organization",
                "name": "INAF - Italian National Institute for Astrophysics"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-6353-0808",
            "givenName": "Volodymyr",
            "familyName": "Savchenko",
            "email": "volodymyr.savchenko@epfl.ch",
            "affiliation": {
                "@type": "Organization",
                "name": "Swiss Federal Institute of Technology Lausanne (EPFL), Rte Cantonale, 1015 Lausanne, Switzerland"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0001-9445-1846",
            "givenName": "John",
            "familyName": "Swinbank",
            "email": "swinbank@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "givenName": "Nico",
            "familyName": "Vermaas",
            "email": "vermaas@astron.nl",
            "affiliation": {
                "@type": "Organization",
                "name": "ASTRON, Oude Hoogeveensedijk 4, 7991 PD, Dwingeloo, The Netherlands"
            }
        },
        {
            "@type": "Person",
            "url": "https://www.research.ed.ac.uk/en/persons/stelios-voutsinas",
            "givenName": "Stelios",
            "familyName": "Voutsinas",
            "email": "Stelios.Voutsinas@ed.ac.uk",
            "affiliation": {
                "@type": "Organization",
                "name": "University of Edinburgh, Old College, South Bridge, Edinburgh EH8 9YL, UK"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-5686-2078",
            "givenName": "Thomas",
            "familyName": "Vuillaume",
            "email": "thomas.vuillaume@lapp.in2p3.fr",
            "affiliation": {
                "@type": "Organization",
                "name": "Univ. Savoie Mont Blanc, CNRS, Laboratoire d'Annecy de Physique des Particules - IN2P3, 74000 Annecy, France"
            }
        },
        {
            "@type": "Person",
            "@id": "https://orcid.org/0000-0002-6275-8242",
            "givenName": "Manuel",
            "familyName": "Parra-Royón",
            "email": "mparra@iaa.es",
            "affiliation": {
                "@type": "Organization",
                "name": "Instituto de Astrofísica de Andalucía - CSIC, Glorieta de la Astronomía s/n, 18008, Granada, Spain"
            }
        }
    ]
}
