# Traefik reverse proxy

This document describes how to deploy a Traefik container via docker compose and host services behind a reverse proxy.
Most of this document is based on the documentation found in the [Traefik Quick start](https://doc.traefik.io/traefik/getting-started/quick-start/).

## Create a docker network

Create a Docker network which can be references as external in other docker compose files. This allows fine grained control over which containers are made available to the web.

```bash
# `traefik_proxy` can be adjusted to fit your own taste
docker network create traefik_proxy
```

## Example Docker Compose file

```yaml
services:
  traefik:
    image: traefik:2.8  # or any later version
    ports:
      - "80:80"       # http
      - "443:443"     # https
      - "8080:8080"   # Traefik Dashboard
    volumes:
      - $PWD/acme.json:/acme.json
      - $PWD/traefik.yml:/etc/traefik/traefik.yml
      - /var/run/docker.sock:/var/run/docker.sock   # see Security considerations below
    networks:
      - traefik_proxy
    restart: always
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.dashboard.rule=Host(`example.com`)  && (PathPrefix(`/api`) || PathPrefix(`/dashboard`))"
      - "traefik.http.routers.dashboard.service=api@internal"
      - "traefik.http.routers.dashboard.entryPoints=dashboard"
      - "traefik.http.routers.dashboard.middlewares=dashauth"   # see middleware definition below
      # Secure the API with basic auth scheme
      # Run the following command: `htpasswd -nbB username password` to generate a string:
      # IMPORTANT: Note the double $, $ is used as a variable in traefik and is escaped by another $
      - "traefik.http.middlewares.dashauth.basicauth.users=username:$$2y$$05$$wjr3YobW87RJBq5e9B1BmuhP6oGB/RjmLvdZ8k32QL1/Hu1z3Hn.m"


networks:
  traefik_proxy:
    external: true
```


## Configuration file

`traefik.yml`

```yaml
entryPoints:
  web:
    address: :80
    http:
      redirections:
        entryPoint:
          to: "websecure"
          scheme: "https"

  websecure:
    address: :443
    http:
      tls:
        certresolver: leresolver

  dashboard:
    address: :8080

providers:
  docker:
    exposedByDefault: false   # explicitly make container available using labels
    network: traefik_proxy    # should match the (external) network created earlier

api:
  dashboard: true

certificatesresolvers:
  leresolver:
    acme:
      email: your-email@example.com   # use an email for notification of cert expiry
      storage: acme.json
      tlsChallenge: {}
```

## Running

Create the certificate store. Be sure to give it proper file permissions to secure file access.

```bash
touch acme.json
chmod 600 acme.json
```

Create a `docker-compose.yml` and a `traefik.yml` using the above templates and then run:
```bash
docker compose up -d
```

## Security Considerations

Mounting `/var/run/docker.sock` in the container may be considered a security risk. Check the [Traefik documentation](https://doc.traefik.io/traefik/providers/docker/#docker-api-access) for options on how to access the Docker API securely.
