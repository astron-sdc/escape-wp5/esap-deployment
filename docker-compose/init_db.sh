#!/bin/bash
set -e

ESAP_SHARED=./shared

if [ ! -f $ESAP_SHARED/esap_ida_config.sqlite3 ]
then
  docker exec -it esap_api cp esap/esap_ida_config.sqlite3 /shared/esap_ida_config.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_batch_config.sqlite3 ]
then
  docker exec -it esap_api cp esap/esap_batch_config.sqlite3 /shared/esap_batch_config.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_rucio_config.sqlite3 ]
then
  docker exec -it esap_api cp esap/esap_rucio_config.sqlite3 /shared/esap_rucio_config.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_uws.sqlite3 ]
then
  docker exec -it esap_api cp esap/esap_uws.sqlite3 /shared/esap_uws.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_accounts_config.sqlite3 ]
then
  docker exec -it esap_api cp esap/esap_accounts_config.sqlite3 /shared/esap_accounts_config.sqlite3
fi

if [ ! -f $ESAP_SHARED/esap_config.sqlite3 ]
then
  docker exec -it esap_api cp esap/esap_config.sqlite3 /shared/esap_config.sqlite3
fi
