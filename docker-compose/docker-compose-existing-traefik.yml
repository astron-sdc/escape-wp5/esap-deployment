## Check the Treafik.md file on how to set up a Traefik instance

services:
  esap_api:
    container_name: esap_api
    image: git.astron.nl:5000/astron-sdc/escape-wp5/esap-api-gateway:latest
    expose:
      - "80000"
    networks:
      - esap_network
      - traefik_proxy
    restart: always
    labels:
      # Enables traefik to connect to the API
      - "traefik.enable=true"

      # Base API path
      - "traefik.http.routers.esap-api.entryPoints=websecure"
      - "traefik.http.routers.esap-api.rule=Host(`${HOSTNAME}`) && PathPrefix(`/esap-api`)"
      - "traefik.http.routers.esap-api.service=esap-api"
      - "traefik.http.services.esap-api.loadbalancer.server.port=8000"

      - "traefik.http.routers.esap-api.tls=true"
      - "traefik.http.routers.esap-api.tls.certresolver=leresolver"

      # Static files served by Django
      - "traefik.http.routers.esap-static_esap.entryPoints=websecure"
      - "traefik.http.routers.esap-static_esap.service=esap-static_esap"
      - "traefik.http.routers.esap-static_esap.rule=Host(`${HOSTNAME}`) && PathPrefix(`/static_esap`)"
      - "traefik.http.services.esap-static_esap.loadbalancer.server.port=8000"

      - "traefik.http.routers.esap-static_esap.tls=true"
      - "traefik.http.routers.esap-static_esap.tls.certresolver=leresolver"

      # route to the '/oidc' authentication
      - "traefik.http.routers.esap-oidc.entryPoints=websecure"
      - "traefik.http.routers.esap-oidc.service=esap-oidc"
      - "traefik.http.routers.esap-oidc.rule=Host(`${HOSTNAME}`) && PathPrefix(`/oidc`)"
      - "traefik.http.services.esap-oidc.loadbalancer.server.port=8000"

      - "traefik.http.routers.esap-oidc.tls=true"
      - "traefik.http.routers.esap-oidc.tls.certresolver=leresolver"
    environment:
      # Links to the RabbitMQ service defined below
      - CELERY_BROKER_URL=amqp://guest@esap_api_rabbitmq_queue:5672

      # Uses environment variables from a .env automatically
      - RUCIO_AUTH_TOKEN
      - RUCIO_HOST
      - RUCIO_AUTH_HOST
      - SECRET_KEY
      - OIDC_OP_JWKS_ENDPOINT
      - OIDC_OP_AUTHORIZATION_ENDPOINT
      - OIDC_OP_TOKEN_ENDPOINT
      - OIDC_OP_USER_ENDPOINT
      - OIDC_RP_CLIENT_ID
      - OIDC_RP_CLIENT_SECRET
      - LOGIN_REDIRECT_URL
      - LOGOUT_REDIRECT_URL
      - LOGIN_REDIRECT_URL_FAILURE
      - OIDC_RENEW_ID_TOKEN_EXPIRY_SECONDS
    volumes:
      # Storage of the sqlite databasefiles
      - $PWD/shared:/shared

  # Front-end
  esap_gui:
    image: git.astron.nl:5000/astron-sdc/escape-wp5/esap-gui:latest
    restart: always
    expose:
      - "80"
    networks:
      - traefik_proxy
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.esap-gui.entryPoints=websecure"
      - "traefik.http.routers.esap-gui.service=esap-gui"
      - "traefik.http.routers.esap-gui.rule=Host(`${HOSTNAME}`) && PathPrefix(`/esap-gui`)"
      - "traefik.http.services.esap-gui.loadbalancer.server.port=80"
      - "traefik.http.routers.esap-gui.tls=true"
      - "traefik.http.routers.esap-gui.tls.certresolver=leresolver"

  # ESAP Worker (no container name, can be scaled!)
  esap_worker:
    image: git.astron.nl:5000/astron-sdc/escape-wp5/esap-worker:latest
    restart: always
    networks:
      - esap_network
    environment:
      - CELERY_BROKER_URL=amqp://guest@esap_api_rabbitmq_queue:5672
      - UWS_HOST=https://${HOSTNAME}/esap-api/uws/

  # MQ Broker
  rabbitmq:
    container_name: esap_api_rabbitmq_queue
    image: rabbitmq:3.9-alpine
    restart: always
    networks:
      - esap_network
    expose:
      - "5672"

networks:
  esap_network:
  traefik_proxy:
    external: true
