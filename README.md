# ESAP Deployment

This repository contains blueprints on how to quickly deploy an instance of ESAP on your own infrastructure.

This documentation is intentionally minimal.
For more information about ESAP itself see [the documentation wiki](https://git.astron.nl/astron-sdc/escape-wp5/esap-api-gateway/-/wikis/home), and for a much wider range of possible installation routes check out the [installation overview](https://git.astron.nl/astron-sdc/escape-wp5/esap-api-gateway/-/wikis/Build-&-Deploy/Installation-Overview).

This repository currently provides two flavours of deployment:

- A Docker Compose file for deploying on a host with Docker.
- A Helm Chart for deployment on a Kubernetes cluster.

## Components

ESAP currently consists of three major components. Their repositories can be found below.

- [ESAP API Gateway](https://git.astron.nl/astron-sdc/escape-wp5/esap-api-gateway): a Django Application in Python.
- [ESAP GUI](https://git.astron.nl/astron-sdc/escape-wp5/esap-gui): a React project in Javascript.
- [ESAP Worker](https://git.astron.nl/astron-sdc/escape-wp5/esap-worker): a background worker in Python using the Celery framework.


## Docker Compose

Install on a host supporting Docker and Docker Compose.
Installation instructions for docker Compose can be found [here](https://docs.docker.com/compose/install/).

### Configuration

The compose file uses the following environment variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| HOSTNAME | Hostname, used in docker-compose for traefik routing | None |
| RUCIO_AUTH_TOKEN | Token used for authenticating with Rucio | "" |
| RUCIO_HOST | URI where to access the Rucio API | https://escape-rucio.cern.ch |
| RUCIO_AUTH_HOST | Rucio authenticaion host URI | https://escape-rucio-auth.cern.ch/ |
| SECRET_KEY | Django secret ([Django docs](https://docs.djangoproject.com/en/4.1/ref/settings/#secret-key)) | `""` (empty string) |
| OIDC_OP_JWKS_ENDPOINT | ODIC JSON Web Key URI | https://iam-escape.cloud.cnaf.infn.it/jwk |
| OIDC_OP_AUTHORIZATION_ENDPOINT | OIDC auth endpoint | https://iam-escape.cloud.cnaf.infn.it/authorize |
| OIDC_OP_TOKEN_ENDPOINT | OIDC token endpoint | https://iam-escape.cloud.cnaf.infn.it/token |
| OIDC_OP_USER_ENDPOINT | OIDC user info endpoint | https://iam-escape.cloud.cnaf.infn.it/userinfo |
| OIDC_RP_CLIENT_ID | OIDC Client ID | "" |
| OIDC_RP_CLIENT_SECRET | OIDC Client Secret | "" |
| LOGIN_REDIRECT_URL | URI where to redirect to after login | http://localhost:8080/esap-gui/login |
| LOGOUT_REDIRECT_URL | URI where to redirect to after logout | http://localhost:8080/esap-gui/logout |
| LOGIN_REDIRECT_URL_FAILURE | URI where to redirect to in case of auth error | http://localhost:8080/esap-gui/error |
| OIDC_RENEW_ID_TOKEN_EXPIRY_SECONDS | OIDC token duration (in seconds) | 43200 |

These can be provided in an `.env` file (see [docker-compose/.env.example](docker-compose/.env.example))


### Reverse proxy options

It is **strongly recommended** to run ESAP behind a reverse proxy.

This repository uses [Traefik](https://traefik.io/), an easy to configure dynamic reverse proxy which also handles TLS (https) termination and certificate management via [Let's Encrypt](https://letsencrypt.org/). 

You can use `docker-compose-local.yml` to quicky deploy local instances of both ESAP and Traefik.
ESAP will be made available at `http://localhost:8080/esap-gui/`.

Alternatively, if you are hosting other web applications on the same node, you may wish to deploy a general-use Traefik instance managed independently of ESAP. 
See [here](docker-compose/traefik.md) for more information on how to configure Traefik.
When you have that set up, you can then use `docker-compose-existing-traefik.yml` to start ESAP.


<!-- Alternatively you may use `nginx`. **TODO** Provide configuration info for nginx. -->



### Bringing the service up

```bash
# replace by docker-compose if using compose V1
# or install compose V2 with the `docker-compose-plugin` package
docker compose up -d
```

For the initial run you need to migrate the databases. You can use the provided [script](docker-compose/init_db.sh) for this, executing in the same directory as the `docker-compose.yml` file.

```bash
./init_db.sh
```

**NOTE:** Sometimes the configuration is not correctly loaded. All SQLite files should be larger then 0 bytes. If not, remove the file and run the init script again.

### Updating the service

```bash
docker compose pull
docker compose up -d --force-recreate
```

## Helm Chart

**Work in progress**


## Contributing

For developer access to this repository, please send a message on the [ESAP channel on Rocket Chat](https://chat.escape2020.de/channel/esap).
